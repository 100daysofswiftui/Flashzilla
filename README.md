# Flashzilla

Flashzilla is an app that helps users learn things using flashcards

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/flashzilla-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- Gestures
- Hit testing
- Combine framework
- Timer
- Reading specific accessibility settings