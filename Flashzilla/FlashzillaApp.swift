//
//  FlashzillaApp.swift
//  Flashzilla
//
//  Created by Pascal Hintze on 18.03.2024.
//

import SwiftUI

@main
struct FlashzillaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
